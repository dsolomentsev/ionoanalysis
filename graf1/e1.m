%trajectory
e1const;
fid=fopen('Jacob.txt');
TrajectoryData=textscan(fid,'%n%n%n%n%n%n%n%s','headerlines',0,'delimiter',';');
fclose(fid);
trLat = TrajectoryData{1,1};
trLon = TrajectoryData{1,2};
trHei = TrajectoryData{1,3};
trX = TrajectoryData{1,4};
trY = TrajectoryData{1,5};
trZ = TrajectoryData{1,6};


%ncdisp('ModelRunGeo-2006-260-000000.nc')
%finfo = ncinfo('ModelRunGeo-2006-260-000000.nc');
%dimNames = {finfo.Dimensions.Name};
%dimMatch = strncmpi(dimNames,'x',1);


ncid = netcdf.open(strcat('ModelRunGeo-' , num2str(year, '%2.4i') ,'-', num2str(dayOfYear, '%2.3i'), ...
	'-', num2str(hour, '%2.2i'), num2str(minute, '%2.2i'), '00.nc'),'NC_NOWRITE');

[~, netCDFDimLen]=netcdf.inqDim(ncid,1);
netCDF_Lon_MaxPoints=netCDFDimLen;
[~, netCDFDimLen]=netcdf.inqDim(ncid,2);
netCDF_Hei_MaxPoints=netCDFDimLen;
[~, netCDFDimLen]=netcdf.inqDim(ncid,3);
netCDF_Lat_MaxPoints=netCDFDimLen;

data = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'Electron_Density'));

%I don't know, but somehow it's not work
%[sX,sY,sZ] = ndgrid(linspace(0,netCDF_Lat_MaxPoints-1),  linspace(0,netCDF_Hei_MaxPoints-1), linspace(0,netCDF_Lon_MaxPoints-1));
[sX,sY,sZ] = ndgrid((1:netCDF_Lat_MaxPoints)-1,(1:netCDF_Hei_MaxPoints)-1,(1:netCDF_Lon_MaxPoints)-1);

 

% Good! But again, set the numbers somewhwere else and name variables like latStep, lonStep and heightStep
% These three also should be determined from the NetCDF file contents - sometimes we will change the model
% grid parameters

F = griddedInterpolant(sX, sY, sZ, data, 'cubic');


Ne = @(arg_lat,arg_lon,arg_hei) ( F(...
    netCDF_Lat_MaxPoints.*(arg_lat-netCDF_Lat_Start)./(netCDF_Lat_End-netCDF_Lat_Start), ...
    netCDF_Hei_MaxPoints.*(arg_hei-netCDF_Hei_Start)./(netCDF_Hei_End-netCDF_Hei_Start), ...
    netCDF_Lon_MaxPoints.*(arg_lon-netCDF_Lon_Start)./(netCDF_Lon_End-netCDF_Lon_Start) ...
    ));


[A,B]=ndgrid(0:graf_maxHeight,(0:(1/cf_countStepsByX):1));
Q=Ne(P1Lat+B.*(P2Lat-P1Lat),P1Lon+B.*(P2Lon-P1Lon),A);
Q(isnan(Q))=0;
figure
contourf(Q,cf_countColors,'LineStyle', 'none');
hold on;
colorbar;
axis([1 cf_countStepsByX 0 graf_maxHeight]);


%The next code is taken from the program, which calculate ray tracing.
%Some variables aren't used now, but it can be useful in the future

Shirota=P1Lat*pi/180.0;
Dolgota=P1Lon*pi/180.0;
Height=0.001; %it's any number, which is not 0
KShirota=P2Lat*pi/180.0;
KDolgota=P2Lon*pi/180.0;
%KAngle=0.5;  %any number, which is not 0
Omega=1;  %any number, which is not 0
sideShift=0; %it's 0 now, but it can be useful in the future
KVec=Omega;

rx= Height.*cos(Shirota).*cos(Dolgota);
ry= Height.*cos(Shirota).*sin(Dolgota);
rz= Height.*sin(Shirota);
rkx= cos(KShirota).*cos(KDolgota);
rky= cos(KShirota).*sin(KDolgota);
rkz= sin(KShirota);
alpha = acos((rx.*rkx+ry.*rky+rz.*rkz)./Height);
rkLen = Height./cos(alpha);
	

start_kx1= KVec.*cos(Shirota).*cos(Dolgota); %the first vector is normal to the Earth surface
start_ky1= KVec.*cos(Shirota).*sin(Dolgota);
start_kz1= KVec.*sin(Shirota);

start_kx2= rkx.*rkLen-rx; %The second vector - parallel to the tangent to the ground
start_ky2= rky.*rkLen-ry;
start_kz2= rkz.*rkLen-rz;
start_kr2len=sqrt(start_kx2.*start_kx2+start_ky2.*start_ky2+start_kz2.*start_kz2);
start_kx2=KVec.*start_kx2./start_kr2len;
start_ky2=KVec.*start_ky2./start_kr2len;
start_kz2=KVec.*start_kz2./start_kr2len;

start_kx3= start_ky1.*start_kz2-start_kz1.*start_ky2; %The third vector - vector product of the previous two vectors
start_ky3= -(start_kx1.*start_kz2-start_kz1.*start_kx2);
start_kz3= start_kx1.*start_ky2-start_ky1.*start_kx2;
start_kr3len=sqrt(start_kx3.*start_kx3+start_ky3.*start_ky3+start_kz3.*start_kz3);
start_kx3=KVec.*start_kx3./start_kr3len;
start_ky3=KVec.*start_ky3./start_kr3len;
start_kz3=KVec.*start_kz3./start_kr3len;

%sideShift (rotate out from the main plane)
%Turn the second vector
start_kx2_shifted = start_kx2.*cos(sideShift)+start_kx3.*sin(sideShift);
start_ky2_shifted = start_ky2.*cos(sideShift)+start_ky3.*sin(sideShift);
start_kz2_shifted = start_kz2.*cos(sideShift)+start_kz3.*sin(sideShift);

start_kx2=start_kx2_shifted;
start_ky2=start_ky2_shifted;
start_kz2=start_kz2_shifted;

%The third vector is again vector product of the previous two vectors, but
%now it take into account the sideShift
start_kx3= start_ky1.*start_kz2-start_kz1.*start_ky2;
start_ky3= -(start_kx1.*start_kz2-start_kz1.*start_kx2);
start_kz3= start_kx1.*start_ky2-start_ky1.*start_kx2;
start_kr3len=sqrt(start_kx3.*start_kx3+start_ky3.*start_ky3+start_kz3.*start_kz3);
start_kx3=KVec.*start_kx3./start_kr3len;
start_ky3=KVec.*start_ky3./start_kr3len;
start_kz3=KVec.*start_kz3./start_kr3len;

%Calculate the projection of ray to the main plane
x0=trX;
y0=trY;
z0=trZ;
temp=-(start_kx3.*x0+start_ky3.*y0+start_kz3.*z0)./(start_kx3.*start_kx3+start_ky3.*start_ky3+start_kz3.*start_kz3);
ProectX=x0+temp.*start_kx3;
ProectY=y0+temp.*start_ky3;
ProectZ=z0+temp.*start_kz3;
LenProect=sqrt(ProectX.*ProectX+ProectY.*ProectY+ProectZ.*ProectZ);
LenProectQ=sqrt(x0.*x0+y0.*y0+z0.*z0);
LenProectQ0=sqrt(x0(1).*x0(1)+y0(1).*y0(1)+z0(1).*z0(1));
LenK1=sqrt(start_kx1.*start_kx1+start_ky1.*start_ky1+start_kz1.*start_kz1);
AngleShift =acos((ProectX.*start_kx1+ProectY.*start_ky1+ProectZ.*start_kz1)./(LenProect.*LenK1));
HeightQQ = sqrt(trX.*trX+trY.*trY+trZ.*trZ);


scatter(cf_countStepsByX.*AngleShift.*REarth./DistanceBetweenPoints+1,HeightQQ./CmInKm-REarth,1,'red');




figure

pointcolors=1+Ne(trLat.*180./pi,trLon.*180./pi,trHei./CmInKm-REarth);
pointcolors(isnan(pointcolors))=0;
scatter3(trLat.*180./pi,trLon.*180./pi,trHei./CmInKm-REarth,2,pointcolors)
hold on
colorbar;
smin=min(P1Lat,P2Lat);
dmin=min(P1Lon,P2Lon);
smax=max(P1Lat,P2Lat);
dmax=max(P1Lon,P2Lon);
axis([smin smax dmin dmax 0 graf_maxHeight]);




figure %plot the Earth and the trajectories

scale=REarth*CmInKm;
[imageTest]=imread('globe2.jpg');
imageTest=imageTest;
[fX,fY,fZ]=sphere(24);
X=fX*scale;
Y=fY*scale;
Z=-fZ*scale;
h = surface(X,Y,Z,imageTest,'FaceColor','texturemap','EdgeColor','none');
rotate(h,[0 0 1],0)
colormap(gray);
hold on
daspect([1 1 1])
view(3)

scatter3(trX(1:psetEveryNPoint:length(trX)),trY(1:psetEveryNPoint:length(trX)),trZ(1:psetEveryNPoint:length(trX)),radiusOfTrajPoints, 'red')
% Add lights.
%light('position',[-1 0 1]);
%light('position',[-1.5 0.5 -0.5], 'color', [.6 .2 .2]);


