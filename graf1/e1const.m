
    year=2012;
    dayOfYear=53;
    hour=6;
    minute=10;


    P1Lat = 48.5;
    P1Lon = 135;

    P2Lat = 51.783;
    P2Lon = 103;

    DistanceBetweenPoints = 2295; %km

    %netCDF parametrs
    %netCDF_Lat_MaxPoints=80;
    %netCDF_Lon_MaxPoints=60;
    %netCDF_Hei_MaxPoints=150;

    netCDF_Lat_Start=-90;
    netCDF_Lat_End=90;
    netCDF_Lon_Start=0;
    netCDF_Lon_End=360;
    netCDF_Hei_Start=87;
    netCDF_Hei_End=2007;


    REarth = 6370.0;
    CmInKm = 100000.0;
    %Graf parametrs
        graf_maxHeight = 600.0;

        %contourf
        cf_countStepsByX = 100;
        cf_countColors = 20;

        %scatter3 on the Earth
        psetEveryNPoint = 4;
        radiusOfTrajPoints = 4;
