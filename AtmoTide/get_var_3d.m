function [V3, lon3, lat3, alt3] = get_var_3d( varname, nc, timeindex, wrap )

 NoData = -9999;
 Re = 6378136.0;
 
 V3 = netcdf.getVar(nc, netcdf.inqVarID(nc, varname),'double');
 
 indexbad = find( V3 == NoData );
 V3(indexbad) = NaN;
 
 lon3 = netcdf.getVar(nc, netcdf.inqVarID(nc, 'Geo_Lon'), 'double');
 lat3 = netcdf.getVar(nc, netcdf.inqVarID(nc, 'Geo_Lat'), 'double');
 alt3 = netcdf.getVar(nc, netcdf.inqVarID(nc, 'Geo_Radius'), 'double');
 
 lon3 = permute( lon3, [3,1,2] ); %[1,3,2]
 lat3 = permute( lat3, [3,1,2] );
 alt3 = permute( alt3, [3,1,2] );
 V3   = permute( V3,   [3,1,2] );
 
 ind = find( lon3 == NoData );
 if ( ~isempty(ind) ) lon3(ind) = NaN; end
 lon3 = rad2deg(lon3);
 
 ind = find( lat3 == NoData );
 if ( ~isempty(ind) ) lat3(ind) = NaN; end
 lat3 = rad2deg(lat3);    
 
 ind = find( alt3 == NoData );
 if ( ~isempty(ind) ) alt3(ind) = NaN; end 
 alt3 = alt3 - Re;
 
 ind = find( V3 == NoData );
 if ( ~isempty(ind) ) V3(ind) = NaN; end

  if ( max(max(max(lon3))) < 300 )
  %    wrap=false;
  end

 sz = size(lat3);
 
 nlons = sz(1);
 nlats = sz(2);
 nalts = sz(3);
% wrap=false;
%wrap = true;
 if (wrap)
    lat3_ = zeros(nlons+1, nlats, nalts);
    lon3_ = lat3;
    alt3_ = lat3;
    V3_   = lat3;
    
    lat3_(1:nlons,:,:) = lat3;
    lon3_(1:nlons,:,:) = lon3;
    alt3_(1:nlons,:,:) = alt3;
    V3_  (1:nlons,:,:) = V3;
    
    lat3_(nlons+1,:,:) = lat3(1,:,:);
    lon3_(nlons+1,:,:) = 360;
    alt3_(nlons+1,:,:) = alt3(1,:,:);
    V3_  (nlons+1,:,:) = V3  (1,:,:);
    
   V3 = V3_;
   lat3 = lat3_;
   lon3=lon3_;
   alt3=alt3_;
end

if ( strcmp(varname,'Model_Variance'))
    [ED, lon3, lat3, alt3] = get_var_3d( 'Electron_Density', nc, timeindex, wrap );
    V3 = sqrt(V3)./(ED + 1E6);
end
