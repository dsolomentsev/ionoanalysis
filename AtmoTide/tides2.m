formIonModelFilename = @(prev_p,year,dayOfYear,hour,minute) (strcat(prev_p , num2str(year, '%2.4i') ,'-', num2str(dayOfYear, '%2.3i'), ...
	'-', num2str(hour, '%2.2i'), num2str(minute, '%2.2i'), '00.nc'));

LocalTime = 22.0; %hour and min

netCDF_Lat_Start=-90;
netCDF_Lat_End=90;
netCDF_Lon_Start=0;
netCDF_Lon_End=360;
netCDF_Hei_Start=80;
netCDF_Hei_End=2007;

time=0;
totalCountFilesInOneDay =144;

doy=266;
for hr = 0:24
for mn = 0:10:50
if(exist(formIonModelFilename('data//ModelRunGeo-', 2006, doy, hr, mn) , 'file'))
    %open the file
    ncid = netcdf.open(formIonModelFilename('data//ModelRunGeo-', 2006, doy, hr, mn), 'NC_NOWRITE');
    %get dimision
    [~, netCDFDimLen]=netcdf.inqDim(ncid,1);
    netCDF_Lon_MaxPoints=netCDFDimLen;
    [~, netCDFDimLen]=netcdf.inqDim(ncid,2);
    netCDF_Hei_MaxPoints=netCDFDimLen;
    [~, netCDFDimLen]=netcdf.inqDim(ncid,3);
    netCDF_Lat_MaxPoints=netCDFDimLen;
    %get data
    data = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'Electron_Density'));
    sHei=(netCDF_Hei_Start:(netCDF_Hei_End-netCDF_Hei_Start)/netCDF_Hei_MaxPoints:netCDF_Hei_End);

    num_col_shift = netCDF_Lon_MaxPoints* (LocalTime/24.0);
    
    time=(hr*60.0+mn)/(1440.0); %time of day fom 0.0 to 1.0
    
    num_sl =  1+mod(floor(num_col_shift - netCDF_Lon_MaxPoints*time),netCDF_Lon_MaxPoints);
    disp(num_sl);
 
    result(:,:,mod(num_sl,netCDF_Lon_MaxPoints)+1)=squeeze(data(:,:,num_sl));
    
    netcdf.close(ncid);
else
    disp(formIonModelFilename('ModelRunGeo-', 2006, doy, hr, mn));
end
end
end  
disp((sHei(1,2,1) - sHei(1,1,1)));
%calculate integrals
result(isnan(result))=0;
TEC = squeeze(sum(result,2)) .* (sHei(1,2,1) - sHei(1,1,1)).*1000.0;
ionoAltsInd = find(squeeze(sHei(1,:,1)) >= 100 & squeeze(sHei(1,:,1)) <= 500);
ionoED = result(:, ionoAltsInd, :); 
IEC =squeeze( sum(ionoED, 2)) .* (sHei(1,2,1) - sHei(1,1,1)).*1000.0;

%create grid for contourf
[ionoLats,ionoLons] = ndgrid( ...
        (netCDF_Lat_Start:180.0/(netCDF_Lat_MaxPoints-1):netCDF_Lat_End),...
        (netCDF_Lon_Start:360.0/netCDF_Lon_MaxPoints:netCDF_Lon_End - 360.0/netCDF_Lon_MaxPoints));
ionoLons(ionoLons > 180) = ionoLons(ionoLons > 180) - 360; 
ionoLons=ionoLons-360.0/netCDF_Lon_MaxPoints;

%draw IEC and TEC
figure;
load coast; hold on;
contourf(ionoLons,ionoLats,IEC/1e16, 100, 'LineStyle', 'none')
plot(long, lat, 'Color', [1 1 1], 'LineWidth', 1);
colormap jet; ylim([-60 60]); colorbar('location', 'SouthOutside');
xlabel('Lon, deg'); ylabel('Lat, deg');
title('Integrated Electron Content, from 100 up to 500 km')

figure;
load coast; hold on;
contourf(ionoLons,ionoLats,TEC/1e16, 100, 'LineStyle', 'none')
plot(long, lat, 'Color', [1 1 1], 'LineWidth', 1);
colormap jet; ylim([-60 60]); colorbar('location', 'SouthOutside');
xlabel('Lon, deg'); ylabel('Lat, deg');
title('TEC')