%V3 = zeros((323-323+1)*(22-20+1)*6, 60, 80 , 150);

formIonModelFilenameDOY = @(prev_path,year,dayOfYear,hour,minute, a, b) (strcat(prev_path , num2str(year, '%2.4i') ,'-', num2str(dayOfYear, '%2.3i'), ...
	'-', num2str(hour, '%2.2i'), num2str(minute, '%2.2i'), '00.nc'));
ind = 0; wt=0;
for doy = 266:266
    wt = wt+1;
    for hr = 0:23
        for mn = 0:10:50
            if(exist(formIonModelFilenameDOY('data//ModelRunGeo-', 2006, doy, hr, mn, 0, 1) , 'file'))
                ind = ind + 1;
                nc = netcdf.open(formIonModelFilenameDOY('data//ModelRunGeo-', 2006, doy, hr, mn, 0, 1), 'NC_NOWRITE');
                %[V3(ind, :, :, :), lon3, lat3, alt3] = get_var_3d( 'Electron_Density', nc, 1, 0);
                [tmp, lon3, lat3, alt3] = get_var_3d( 'Electron_Density', nc, 1, 0);
                if(ind == 1)
                    V3 = nan(size(tmp));
                    lon1 = squeeze(lon3(:,1,1)); lon1(lon1>180)=lon1(lon1>180)-360;
                   
                else
                    LT = hr+mn/60+lon1*24/360; LT(LT<0) = LT(LT<0)+24;
                    [~,ind22] = min(abs(LT-22));
                    %ind22 = find(LT <= 22 & LT >= 20);
                    V3(ind22, :, :) = tmp(ind22, :, :);
                    %V3(ind, :, :, :)  = tmp;
                end
                netcdf.close(nc);
            end
        end
    end
    if(wt == 1)
        V3_all = V3;
        %V3_all = squeeze(mean(V3, 1));
    else
        V3_all = (wt.*V3_all + V3)./(wt+1);
    end
end
%V3 = squeeze(mean(V3, 1));
%% Get the Ionosphere Electron Content and TEC
V3_all(isnan(V3_all))=0;
TEC = squeeze(sum(V3_all,3))*(alt3(1,1,2) - alt3(1,1,1));
%TEC(isnan(TEC))=0;
ionoAltsInd = find(squeeze(alt3(1,1,:)) >= 100e3 & squeeze(alt3(1,1,:)) <= 500e3);
ionoED = V3_all(:, :, ionoAltsInd); ionoLons = squeeze(lon3(:, :, 1));
ionoLats = squeeze(lat3(:, :, 1)); ionoAlts = alt3(:, :, ionoAltsInd);
ionoLons(ionoLons > 180) = ionoLons(ionoLons > 180) - 360;  

IEC = squeeze(sum(ionoED, 3))*(alt3(1,1,2) - alt3(1,1,1));
%for i = 1:80
%    IEC(:, i)=smooth(IEC(:, i), 10);
%end

%% Plot IEC
figure;
load coast; hold on;
contourf(ionoLons, ionoLats, IEC/1e16, 100, 'LineStyle', 'none')
plot(long, lat, 'Color', [1 1 1], 'LineWidth', 1);
colormap jet; ylim([-60 60]); colorbar('location', 'SouthOutside');
xlabel('Lon, deg'); ylabel('Lat, deg');
title('Integrated Electron Content, from 100 up to 500 km')
%% Plot TEC
figure;
load coast; hold on;
contourf(ionoLons, ionoLats, TEC/1e16, 100, 'LineStyle', 'none')
plot(long, lat, 'Color', [1 1 1], 'LineWidth', 1);
colormap jet; ylim([-60 60]); colorbar('location', 'SouthOutside');
xlabel('Lon, deg'); ylabel('Lat, deg');
title('TEC')
